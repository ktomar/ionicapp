angular.module('jot.services', [])

/**
 * A simple example service that returns some data.
 */
.factory('JotFactory', function ($http, CONFIG) {

    this.oauthToken = function ()
    {
        //withCredentials: true
        return $http({ method: 'GET', url: CONFIG.backendURL + '/oauth/token' });
    }

    this.login = function (code, provider) {
        return $http({
            method: 'POST',
            url: CONFIG.backendURL + '/signin',
            data: $.param({ code: code, provider: provider }),
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            //withCredentials: true
        });
    }

    this.getPosts = function () {
        return $http({ method: 'GET', url: CONFIG.backendURL + '/list' });
    }

    this.getPostsByTag = function (hashtag) {
        return $http({ method: 'GET', url: CONFIG.backendURL + '/list/' + hashtag });
    }

    this.browseHashtags = function () {
        return $http({ method: 'GET', url: CONFIG.backendURL + '/hashtags' });
    }

    this.getPostById = function (postid) {
        return $http({ method: 'GET', url: CONFIG.backendURL + '/view/' + postid });
    }

    this.addPost = function (text) {
        return $http({method: 'POST', 
            url:CONFIG.backendURL + '/add', 
            data: { text: text }
        });
    }
  
    return {
        //oauth
        getToken: this.oauthToken,
        login: this.login, 
        list: this.getPosts,
        listByTag: this.getPostsByTag,
        browse: this.browseHashtags,
        view: this.getPostById,
        add: this.addPost
  }
});
