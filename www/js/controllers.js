angular.module('jot.controllers', [])

.controller('LoginCtrl', function ($scope, $location, CONFIG, JotFactory) {
    $scope.WelcomeMessage = '';
    $scope.login = function (provider) {
        OAuth.initialize(CONFIG.oauth_key);
        //  if (CONFIG.phone) {
        JotFactory.getToken().success(function (data) {
            OAuth.popup(provider, { state: data.token, cache: true }).done(function (result) {
                // callback function
                $location.path("/tab/latest");
                result.me().done(function (response) {
                    $scope.$apply(function () {
                        $scope.WelcomeMessage = 'Welcome ' + response.firstname + ' ' + response.lastname;
                    });

                }).fail(function (err) {
                    $scope.WelcomeMessage = "Get my info error: " + err;
                });
            }).fail(function (error) {
                $scope.$apply(function () {
                    $scope.WelcomeMessage = "Authuntication Error :" + error;
                });
            });
        });
    }
    //  else {
    // JotFactory.getToken().success(function (data) {
    //OAuth.popup(provider, {
    //    state: data.token
    //}).done(function (r) {
    //    //JotFactory.login(r.code).success(function (data, status) {
    //    //    console.log(data);
    //    //    $location.path("/tab/latest");
    //    //}).error(function (data, status) {
    //    //    console.log('backend error');
    //    //    console.log(data);
    //    //});
    //}).fail(function (e) {
    //    console.log('popup error');
    //    console.log(e);
    //    $scope.WelcomeMessage = e;
    //});
    // });
    // }
})

.controller('PostsListCtrl', function ($scope, $rootScope, $stateParams, JotFactory) {
    if ($stateParams.hashtag != null) {
        $scope.title = $stateParams.hashtag;
        $rootScope.$watch('add', function () {
            JotFactory.listByTag($stateParams.hashtag).success(function (data) {
                $scope.posts = data;
            });
        });        
        console.log($scope.posts);
    }
    else {
        $scope.title = "Home";
        $rootScope.$watch('add', function () {
            JotFactory.list().success(function (data) {
                $scope.posts = data;
            });
        });
    }
    
})

.controller('PostviewCtrl', function ($scope, $stateParams, JotFactory) {
    JotFactory.view($stateParams.postid).success(function (data) {
        $scope.post = data[0];
    });
})

.controller('HashtagsCtrl', function ($scope, JotFactory) {
    JotFactory.browse().success(function (data) {
        $scope.hashtags= data;
    });
})

.controller('AddPostCtrl', function ($scope, $location, JotFactory) {
    $scope.post = {text: ''};
    $scope.savePost = function (isValid) {
        if (isValid) {
            JotFactory.add($scope.post.text).success(function (data) {
                var newid = data._id;
                $location.path("/tab/view/" + newid);
            });
        }
    }
});

