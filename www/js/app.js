// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('jot', ['ionic', 'jot.controllers', 'jot.services', 'jot.directives', 'jot.constant'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
       // setup an abstract state for the tabs directive
      .state('auth', {
          url: "/auth",
          abstract: true,
          templateUrl: "templates/auth.html"
      })

          .state('tab', {
              url: "/tab",
              abstract: true,
              templateUrl: "templates/tabs.html"
          })


       // Each tab has its own nav history stack:
      // Authentication routes
      .state('auth.login', {
          url: '/login',
          views: {
              'login': {
                  templateUrl: 'templates/auth_login.html',
                  controller: 'LoginCtrl'
              }
          }
      })

      // Jot routes
    .state('tab.latest', {
        url: '/latest',
      views: {
          'latest': {
              templateUrl: 'templates/view_postslist.html',
              controller: 'PostsListCtrl'
        }
      }
    })

    .state('tab.add', {
      url: '/add',
      views: {
          'postadd': {
          templateUrl: 'templates/view_postadd.html',
          controller: 'AddPostCtrl'
        }
      }
    })

        .state('tab.hash', {
            url: '/hash/:hashtag',
            views: {
                'latest': {
                    templateUrl: 'templates/view_postslist.html',
                    controller: 'PostsListCtrl'
                }
            }
        })

    .state('tab.view', {
      url: '/view/:postid',
      views: {
          'latest': {
            templateUrl: 'templates/view_postview.html',
            controller: 'PostviewCtrl'
        }
      }
    })

     .state('tab.trending', {
         url: '/browse',
         views: {
             'hashtag': {
                 templateUrl: 'templates/view_hashtag.html',
                 controller: 'HashtagsCtrl'
             }
         }
     })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/auth/login');

});

