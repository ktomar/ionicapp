## Installation:

Open command prompt and navigate to the project directory 

Run the following command to install required modules:

```bash
 $ npm install
```

## Configure the application:

Open the application directory in windows explorer then go to \www\config
  
  - Open config.js
  - Change "backendURL" key to the backend URL ex:https://jot-backend.herokuapp.com (Don't add trailing slash)
  - Change "oauth_key" key to your oauth.io Public key
  

## Testing the application on Emulator:

Open command prompt and navigate to the project directory and run the following: 
```bash
 $ ionic platform add android
 
 $ ionic build android
 
 $ ionic emulate android
```


To install and test the application on android device, connect your mobile to computer through USB cable and replace the last command with the following command:
```bash
 $ ionic run android
```
Note: You must enable USB debugging on your mobile device
